Pinky QR
========

## これは何？

QR コードを読み込むアプリ。
iOS アプリ作りの練習として作ってみました。

## 特徴

* 若年層の女性向け - iOS のデザインを元にですが、所々ピンク色にしています。

## 著作権・ライセンス

© 2016 [yuta*ﾟ](https://y59.jp/), Some Rights Reserved.
ライセンスは [Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) とします。
