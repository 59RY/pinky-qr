/**
 * Read_ViewController.swift
***/

import UIKit
import AVFoundation

class ReadViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Create a Session
		let mySession: AVCaptureSession! = AVCaptureSession()
		
		// List Devices
		let devices = AVCaptureDevice.devices()
		
		// ...and Put These
		var myDevice: AVCaptureDevice!
		
		// Also Put Rear-Camera(s).
		for device in devices{
			if(device.position == AVCaptureDevicePosition.Back){
				myDevice = device as! AVCaptureDevice
			}
		}
		
		// Get a Input from Rear-Camera(s)
		var myVideoInput = AVCaptureDeviceInput?()
		do{
			myVideoInput = try AVCaptureDeviceInput(device: myDevice)
		} catch{
			myVideoInput = nil
		}
		
		// Can Access Camera?
		if mySession.canAddInput(myVideoInput) {
			
			// Add to a Session
			mySession.addInput(myVideoInput)
			
			// Output MetaDeta
			let myMetadataOutput: AVCaptureMetadataOutput! = AVCaptureMetadataOutput()
			
			if mySession.canAddOutput(myMetadataOutput) {
				// Add to a Session
				mySession.addOutput(myMetadataOutput)
				// Set Delegate When Get a MetaData
				myMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
				// Set QRCode
				myMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
			}
			
			// Create a Video Layer
			let myVideoLayer : AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: mySession) as AVCaptureVideoPreviewLayer
			myVideoLayer.frame = self.view.bounds
			myVideoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
			
			// Add to a View
			self.view.layer.addSublayer(myVideoLayer)
			
			// Start to Run!
			mySession.startRunning()
			
		} else{
			
			// Alert that cannot access the camera
			cameraUnavailable()
			
		}

	}
	
	
	func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
		if metadataObjects.count > 0 {
			let qrData: AVMetadataMachineReadableCodeObject  = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
			
			// Launch Safari
			UIApplication.sharedApplication().openURL(NSURL(string: qrData.stringValue)!)
		}
	}
	
	
	// If cannot access the camera... ｡･ﾟ(ﾟ⊃ω⊂ﾟ)ﾟ･｡
	func cameraUnavailable(){
		
		// Alert Message
		let alert = UIAlertController(
			title:
				NSLocalizedString("Unavailable Camera Access", comment: "Unavailable Camera Access"),
			message:
				NSLocalizedString("Please change a privilege.", comment: "Unavailable Camera Message"),
			preferredStyle: .Alert
		)
		
		// Button 1: Guide to Settings App
		let btn_1 = UIAlertAction(
			title: NSLocalizedString("Go to Settings App.", comment: "Go to Settings App."),
			style: .Destructive
		){
			(_:UIAlertAction!) -> Void in let settings_url = NSURL(string: "app-settings:")
			UIApplication.sharedApplication().openURL(settings_url!)
		}
		alert.addAction(btn_1)
		
		// Button Exit
		let btn_exit = UIAlertAction(
			title:   NSLocalizedString("I'll do later", comment: "I'll do later"),
			style:   .Default,
			handler: nil
		)
		alert.addAction(btn_exit)
		
		// Alert
		self.presentViewController(alert, animated: true, completion: nil)
		
	}
	
	
}
