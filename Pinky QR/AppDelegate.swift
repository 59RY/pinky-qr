/**
 * AppDelegate.swift
 * Manage life-time-events.
***/

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		//:: Override point for customization after application launch.
		return true
	}
	
	func applicationWillEnterForeground(application: UIApplication) {
		//:: When the App. is gonna active
	}
	
	func applicationDidBecomeActive(application: UIApplication) {
		//:: When the App. is actived
	}
	
	func applicationWillResignActive(application: UIApplication) {
		//:: When the App. is gonna inactive
	}
	
	func applicationDidEnterBackground(application: UIApplication) {
		//:: When the App. is inactived
	}

	func applicationWillTerminate(application: UIApplication) {
		//:: When the App. is closed explicitly
	}

}


class PinkTabBar: UITabBarController {
 
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Set the color. Of course, it's pink (*ﾉωﾉ)♥
		let colorKey = UIColor(red: 255/255, green: 105/255, blue: 230/255, alpha: 1.0)
		let colorBG  = UIColor(red: 255/255, green: 245/255, blue: 253/255, alpha: 0.8)
		
		// Change the font-color.
		let selectedAttributes: [String : AnyObject]? = [NSForegroundColorAttributeName: colorKey]
		UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, forState: UIControlState.Selected)
		
		// Change the background-color.
		UITabBar.appearance().barTintColor = colorBG
		
		// Change the icon-color.
		UITabBar.appearance().tintColor = colorKey
		
	}
 
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
}